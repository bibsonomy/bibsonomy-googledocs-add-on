/**
 * Fetches Bibtex data from the BibSonomy API. This fetches all publications of
 * the given user that match the search terms.
 * 
 * @param {String}
 *            bibsonmyUserName - The user name to fetch data from
 * @param {String}
 *            searchTerm - The search terms
 * @returns {String} JSON string with the publication data.
 */
BibSonomyFetch.prototype.fetchFromUser = function(bibsonmyUserName, searchTerm, offset) {
	var userPosts = {};
	var step = 1000;
	var url = 'http://www.bibsonomy.org/api';
	
	// if a user name is given, fetch from that user. otherwise fetch globally
	if (bibsonmyUserName != "")
		url += '/users/' + bibsonmyUserName;
	
	url += '/posts?resourcetype=bibtex&start=' + offset + '&end=' + (offset + step);
	
	if (searchTerm != "")
		url += '&search=' + escape(searchTerm);

	var nameFormatter = new NameFormatter();
	var options = this.getApiOptions();
	
	var response = UrlFetchApp.fetch(url + '&format=json', options);
	var parsedJSON = JSON.parse(response.getContentText());

	var newUserPosts = parsedJSON.posts.post;
	for ( var attrname in newUserPosts) {
		var authors = newUserPosts[attrname].bibtex.author;
		var id = (parseInt(attrname) + offset).toString();
		userPosts[id] = newUserPosts[attrname];
		if (authors !== undefined) {
			userPosts[id].bibtex.authorsLastNames = nameFormatter.formatLastNames(authors);
			userPosts[id].bibtex.author = nameFormatter.formatAuthors(authors);
			userPosts[id].bibtex.authorShort = nameFormatter.formatAuthorsShort(authors);
        }
	}

	url = parsedJSON.posts.next;
	offset = offset + step;
	
	var result = { posts: JSON.stringify(userPosts), nextStart: url === undefined ? 0 : offset };
	return result;
}

/**
 * Fetches Bibtex data from the BibSonomy API. This fetches all publications of
 * the current user that match the search terms.
 * 
 * @param {String}
 *            searchTerm - The search terms
 * @returns {String} JSON string with the publication data.
 */
BibSonomyFetch.prototype.fetchOwn = function(searchTerm, offset) {
	return this.fetchFromUser(this.userName,searchTerm,offset);
}

/**
 * Fetches Bibtex data from the BibSonomy API. This fetches all public publications
 * that match the search terms.
 * 
 * @param {String}
 *            searchTerm - The search terms
 * @returns {String} JSON string with the publication data.
 */
BibSonomyFetch.prototype.fetchPublic = function(searchTerm, offset) {
	return this.fetchFromUser("",searchTerm,offset);
}

/**
 * Returns the user details for the given user
 */
BibSonomyFetch.prototype.fetchUserInfo = function(userName) {
	var url = 'http://www.bibsonomy.org/api/users/' + userName;
	var options = this.getApiOptions();
	
	var response = UrlFetchApp.fetch(url + '?format=json', options);
	return JSON.parse(response.getContentText());
}

/**
 * Returns the specified document from the specified user
 */
BibSonomyFetch.prototype.fetchDocument = function(userName, intrahash) {
	var url = 'http://www.bibsonomy.org/api/users/' + userName + '/posts/' + intrahash;
	var options = this.getApiOptions();

	var response = UrlFetchApp.fetch(url + '?format=json', options);
	var parsedJSON = JSON.parse(response.getContentText());

	var post = parsedJSON.post;
	if (post.bibtex.author !== undefined) {
		var nameFormatter = new NameFormatter();
		post.bibtex.author = nameFormatter.formatAuthors(post.bibtex.author);
	}

	return post;
}

/**
 * Creates an options object for fetching data from the BibSonomy API using API
 * key.
 * 
 * @param {String}
 *            userName - The user name to fetch data from.
 * @param {String}
 *            apiKey - The API key for the user.
 * @returns {Object} Options for fetching data from the API.
 */
BibSonomyFetch.prototype.getApiOptions = function() {
	var headers = {
		"muteHttpExceptions" : true,
		"User-Agent" : "GoogleAppsTestScript",
		"Authorization" : "Basic "
				+ Utilities.base64Encode(this.userName + ":" + this.apiKey)
	};
	var options = {
		'headers' : headers
	};
	return options;
}

/**
 * Creates an options object for fetching data from the BibSonomy API using
 * OAuth.
 * 
 * @returns {Object} Options for fetching data from the API.
 */
BibSonomyFetch.prototype.getOAuthOptions = function() {
	var consumerKey = 'googledoc';
	var consumerSecret = 'ohx4Dulu eM3zooCh';

	var oauthConfig = UrlFetchApp.addOAuthService('bibsonom5');
	oauthConfig.setAccessTokenUrl('http://www.bibsonomy.org/oauth/accessToken');
	oauthConfig
			.setRequestTokenUrl('http://www.bibsonomy.org/oauth/requestToken');
	oauthConfig.setAuthorizationUrl('http://www.bibsonomy.org/oauth/authorize');
	oauthConfig.setConsumerKey(consumerKey);
	oauthConfig.setConsumerSecret(consumerSecret);

	var options = {
		muteHttpExceptions : true,
		'oAuthServiceName' : 'bibsonom5',
		'oAuthUseToken' : 'always'
	};
	return options;
}

BibSonomyFetch.prototype.setLogin = function(userName, apiKey)
{
	this.userName = userName;
	this.apiKey = apiKey;
}

/**
 * Constructor for BibSonomyFetch.
 * 
 * @returns {BibSonomyFetch} The new BibSonomyFetch object.
 */
function BibSonomyFetch() {
	this.userName = "";
	this.apiKey = "";
}
