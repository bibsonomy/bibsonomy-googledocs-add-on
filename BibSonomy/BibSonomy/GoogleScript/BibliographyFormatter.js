
/**
 * Adds a document. getCiteText() is not called before all documents
 * were added
 */
BibliographyFormatter.prototype.addDocument = function(id, publication) {
	if (publication == undefined)
		return;
	
	var value = { pub: publication, index: this.citeCounter++ };
	this.publications[id] = value;
}

/**
 * Called after all documents were added. Assigns indices
 * according to the sort order
 */
BibliographyFormatter.prototype.sortDocuments = function() {
	if (getBibliographySortType() == "name") {
		// create intermediate array
		var array = [];
		for (var key in this.publications) {
			var doc = this.publications[key];
			var elem = { id: key, pub: doc.pub }
			array.push(elem);
		}
		
		// sort by author name
		array.sort(function(a, b) {
			if (a.pub.authorsLastNames == undefined)
				return -1;
		    return a.pub.authorsLastNames.localeCompare(b.pub.authorsLastNames);
		});
		
		// apply indices to map
		for (var i = 0; i < array.length; i++) {
			this.publications[array[i].id].index = i;
		}
	}
}

/**
 * Returns the text to use for a citation
 */
BibliographyFormatter.prototype.getCiteText = function(id) {
	var elem = this.publications[id];
	if (elem == undefined)
		return undefined;
	
	return "[" + (elem.index+1) + "]";
}

/**
 * Returns an array with the document ids, in the order they should
 * appear in the bibliography
 */
BibliographyFormatter.prototype.getBibliography = function() {
	// create intermediate array
	var array = [];

	for (var key in this.publications) {
		var doc = this.publications[key];
		array[doc.index] = key;
	}
	
	return array;
}

function BibliographyFormatter() {
	this.publications = {};
	this.citeCounter = 0;
}