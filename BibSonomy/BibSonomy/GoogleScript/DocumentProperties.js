
function getUserName() {
	var userProperties = PropertiesService.getUserProperties();
	var userName = userProperties.getProperty('BibSonomy-userName');
	if (userName === undefined) {
		userName = "";
	}
	return userName;
}

function setUserName(userName) {
	var userProperties = PropertiesService.getUserProperties();
	userProperties.setProperty('BibSonomy-userName',userName);
}

function getApiKey() {
	var userProperties = PropertiesService.getUserProperties();
	return userProperties.getProperty('BibSonomy-API-Key');
}

function setApiKey(apiKey) {
	var userProperties = PropertiesService.getUserProperties();
	userProperties.setProperty('BibSonomy-API-Key',apiKey);
}

function getAutoLoad() {
	var userProperties = PropertiesService.getUserProperties();
	return userProperties.getProperty('BibSonomy-AutoLoad');
}

function setAutoLoad(autoLoad) {
	var userProperties = PropertiesService.getUserProperties();
	userProperties.setProperty('BibSonomy-AutoLoad',autoLoad);
}

function getUserProperties() {
	var result = {};

	result["userName"] = getUserName();
	result["apiKey"] = getApiKey();
	result["autoLoad"] = getAutoLoad();
	
	return result;
}

function setUserProperties(props) {
	setUserName(props.userName);
	setApiKey(props.apiKey);
	setAutoLoad(props.autoLoad);
}




function getLockStatus() {
	var documentProperties = PropertiesService.getDocumentProperties();
	return documentProperties.getProperty('lock');
}

function getInsertLinks() {
	var documentProperties = PropertiesService.getDocumentProperties();
	return documentProperties.getProperty('BibSonomy-InsertLinks');
}

function setInsertLinks(insertLinks) {
	var documentProperties = PropertiesService.getDocumentProperties();
	documentProperties.setProperty('BibSonomy-InsertLinks',insertLinks);
}

function getBibliographySortType() {
	var documentProperties = PropertiesService.getDocumentProperties();
	var type = documentProperties.getProperty('BibSonomy-BibliographySortType');
	if (type == undefined)
		type ="pos";
	return type;
}

function setBibliographySortType(bibliographyType) {
	var documentProperties = PropertiesService.getDocumentProperties();
	documentProperties.setProperty('BibSonomy-BibliographySortType',bibliographyType);
}

function getAbbreviateFirstName() {
	var documentProperties = PropertiesService.getDocumentProperties();
	return documentProperties.getProperty('BibSonomy-AbbreviateFirstName');
}

function setAbbreviateFirstName(abbreviate) {
	var documentProperties = PropertiesService.getDocumentProperties();
	documentProperties.setProperty('BibSonomy-AbbreviateFirstName',abbreviate);
}

function getDocumentProperties() {
	var result = {};

	result["insertLinks"] = getInsertLinks();
	result["bibliographySortType"] = getBibliographySortType();
	result["abbreviateFirstName"] = getAbbreviateFirstName();
	
	return result;
}

function setDocumentProperties(props) {
	setInsertLinks(props.insertLinks);
	setBibliographySortType(props.bibliographySortType);
	setAbbreviateFirstName(props.abbreviateFirstName);
}
