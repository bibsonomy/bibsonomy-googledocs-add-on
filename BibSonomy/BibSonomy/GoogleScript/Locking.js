// Locking class for locking the document from concurrent use. 

/*
 * Locks the document to prevent changes
 */
Locking.prototype.lock = function() {
  var documentProperties = PropertiesService.getDocumentProperties();
  documentProperties.setProperty('lock', 'true');
}

/*
 * Restore locks to allow changes
 */
Locking.prototype.unlock = function() {
  var documentProperties = PropertiesService.getDocumentProperties();
  documentProperties.setProperty('lock', 'false')
}

/*
 * Restore document locks to the default value
 */
function resetLocks() {
  Locking.prototype.unlock();
}  

function Locking() {
};