/**
 * This function modifies the format of a string name and returns it.
 * 
 * @param {String}
 *            name - Name in format "last_name, first_name".
 * @returns {String} Name in format "first_name last_name".
 */
NameFormatter.prototype.formatName = function(name) {
	var split = name.split(", ");
	var first = split[1];
	var last = split[0];
	return first + " " + last;
};

NameFormatter.prototype.formatNameShort = function(name) {
	var split = name.split(", ");
	var first = split[1];
	if (first != undefined)
		first = first.substring(0,1) + ".";
	var last = split[0];
	return first + " " + last;
};

/**
 * This function modifies the format of the string names and returns an new
 * string with the modified names.
 * 
 * @param {String}
 *            authors - Names in format "last_name1, first_name1 and last_name2,
 *            first_name2 and last_name3, first_name3".
 * @returns {String} Names in format "first_name1 last_name1, first_name2
 *          last_name2 and first_name3 last_name3".
 */
NameFormatter.prototype.formatAuthors = function(authors) {
	authorList = authors.split(" and ");
	resultList = [];
	result = "";
	for (var i = 0; i < authorList.length; i++) {
		resultList.push(this.formatName(authorList[i]));
	}
	result = resultList[0];
	if (resultList.length > 1) {
		for (var i = 1; i < resultList.length - 1; i++) {
			result = result + ", " + resultList[i];
		}
		for (var i = resultList.length - 1; i < resultList.length; i++) {
			result = result + " and " + resultList[i];
		}
	}
	return result;
};

NameFormatter.prototype.formatAuthorsShort = function(authors) {
	authorList = authors.split(" and ");
	resultList = [];
	result = "";
	for (var i = 0; i < authorList.length; i++) {
		resultList.push(this.formatNameShort(authorList[i]));
	}
	result = resultList[0];
	if (resultList.length > 1) {
		for (var i = 1; i < resultList.length - 1; i++) {
			result = result + ", " + resultList[i];
		}
		for (var i = resultList.length - 1; i < resultList.length; i++) {
			result = result + " and " + resultList[i];
		}
	}
	return result;
};

/**
 * This function modifies the format of the string names and returns a new
 * string with only the last names.
 */
NameFormatter.prototype.formatLastNames = function(authors) {
	var authorList = authors.split(" and ");
	var result = "";

	for (var i = 0; i < authorList.length; i++) {
		var name = authorList[i].split(", ")[0];
		
		result = result + name;
		if (i+1 != authorList.length)
			result = result + ", ";
	}
	
	return result;
}
/**
 * Constructor for NameFormatter.
 * 
 * @returns {NameFormatter} The new NameFormatter object.
 */
function NameFormatter() {

}