var map = {};
var citeCounter = 0;
var userPosts;

// used to store bookmarks
var bookmarkReferences = {};

/**
 * Encodes the given publication into a string to identify it
 * @returns {String}
 */
function encodeDocumentId(publication) {
	return 'BibSonomyCite-' + publication.user.name + '-' + publication.bibtex.intrahash;
}

/**
 * Checks if the given string is a document id
 */
function isDocumentId(id) {
	return id.lastIndexOf("BibSonomyCite-", 0) === 0;
}

/**
 * Returns a structure with the elements name and intrahash, or undefned if the
 * given id is invalid
 */
function decodeDocumentId(id) {
	var result = undefined;
	
	if (id != undefined && id.lastIndexOf("BibSonomyCite-", 0) === 0) {
		var parts = id.split('-');
		if (parts.length === 3) {
			result = { name: parts[1], intrahash: parts[2] };
		}
	}
	
	return result;
}

var publicationCache = {};

function getPublicationFromCache(id) {
	// check if the document is already known
	if (id in publicationCache)
		return publicationCache[id];
	
	Logger.log('Caching document ' + id);
	
	// decode id
	var metaData = decodeDocumentId(id);
	if (metaData == undefined)
		return undefined;
	
	// load document and filter attributes
	var publication = fetchDocumentFromBibsonomy(metaData.name,metaData.intrahash);
	publication = filterBibtexData(publication);
	
	// remember it for later
	publicationCache[id] = publication;

	// write to properties
	var documentProperties = PropertiesService.getDocumentProperties();
	documentProperties.setProperty(id, JSON.stringify(publication));
	
	return publication;
}

function addPublicationToCache(publication) {
	var id = encodeDocumentId(publication);
	if (id in publicationCache)
		return;
	
	var pub = filterBibtexData(publication);
	publicationCache[id] = pub;
	
	// write to properties
	var documentProperties = PropertiesService.getDocumentProperties();
	documentProperties.setProperty(id, JSON.stringify(pub));
}

/**
 * Loads the citationData from the documentProperties Storage into the variable
 * citesInDocument.
 */
function loadStoredCitationData() {

	// The document properties store can save a total of 500kb.
	// Each key-value property may use up to 9kb.
	// Because of that we'll store each used document as a seperate property.

	var documentProperties = PropertiesService.getDocumentProperties();
	var storedData = documentProperties.getProperties();
	publicationCache = {};

	for ( var key in storedData) {
		Logger.log(key, storedData[key]);
		if (storedData[key] != 'undefined' && decodeDocumentId(key) != undefined) {
			publicationCache[key] = JSON.parse(storedData[key]);
		} else if (key.lastIndexOf('BibSonomy-',0) != 0) {
			Logger.log("Deleted invalid property: " + key);
			documentProperties.deleteProperty(key);
		}
	}
}

function updateRanges() {
	var locked = getLockStatus();
	if (locked == 'true') {
		DocumentApp
				.getUi()
				.alert(
						"Already performing an operation, please wait a few seconds and try again.");
		return;
	}
	
	var AllRanges = DocumentApp.getActiveDocument().getNamedRanges();
	if (!AllRanges) {
		DocumentApp.getUi().alert('Cannot find ranges in the document.');
		return;
	}
	
	Locking.prototype.lock();
  
	loadStoredCitationData();
	var bibliographyTable = getBibliographyTableElement();
	bibliographyTable.clear();
	bookmarkReferences = {};
	
	// initialize bibliography formatter
	var formatter = new BibliographyFormatter();
	var ranges = [];
	var rangeCounter = 0;  
	for (var i = 0; i < AllRanges.length; ++i) {
		var selectedElement = AllRanges[i];
		var selEleName = selectedElement.getName();

		// Checks if the element name starts with BibSonomyCite-, if not, it
		// will be skipped
		if (isDocumentId(selEleName)) {
			ranges[rangeCounter++] = selectedElement;
			
			var pub = getPublicationFromCache(selEleName);
			formatter.addDocument(selEleName,pub);
		}
	}
	
	formatter.sortDocuments();
	
	// create bibliography
	var bib = formatter.getBibliography();
	var map = {};
	for (var i = 0; i < bib.length; i++) {
		var id = bib[i];

		var citeText = formatter.getCiteText(id);
		insertReference(bibliographyTable,id,citeText);
		
		map[id] = citeText;
	}
	
	// insert citations
	var insertLinks = getInsertLinks() == 'true';
	for (var index = 0; index < ranges.length; index++) {
		var selectedElement = ranges[index];
		var selEleName = selectedElement.getName();

		var valid = true;

		// check if this reference was found before
		var citeText = formatter.getCiteText(selEleName);
		if (citeText == undefined) {
			citeText = '[?]';
			valid = false;
		}

		// delete old text and replace it
		var text = selectedElement.getRange().getRangeElements()[0]
				.getElement().editAsText();
		var pos = selectedElement.getRange().getRangeElements()[0]
				.getStartOffset();
		var end = selectedElement.getRange().getRangeElements()[0]
				.getEndOffsetInclusive();
		text.insertText(end, citeText);
		text.deleteText(pos, end - 1);
		text.deleteText(end, end);
		
		// insert link to bibliography
		if (valid && bookmarkReferences[citeText] != undefined && insertLinks) {
			text.setLinkUrl(pos, end, "#bookmark="
					+ bookmarkReferences[citeText]);
		} else {
			text.setLinkUrl(pos, end, null);
		}
	}
	
	// Remove unused documents from the storage.
	var documentProperties = PropertiesService.getDocumentProperties();
	var storedData = documentProperties.getProperties();
	for ( var key in storedData) {
		if (!(key in map) && key != "lock" && key.lastIndexOf('BibSonomy-',0) != 0) {
			documentProperties.deleteProperty(key);
		}
	}
  
	Locking.prototype.unlock();
}

function getCiteNumber(hashId) {
	if (hashId in map) {
		return map[hashId];
	} else {
		citeCounter++;
		map[hashId] = "[" + citeCounter + "]";
		return map[hashId];
	}
}

function insertBetter(position, textToInsert, isBold, isItalic) {
	var pos = position.getText().length;
	position.insertText(pos, textToInsert);
	position.setBold(pos, pos + textToInsert.length - 1, isBold);
	position.setItalic(pos, pos + textToInsert.length - 1, isItalic);
}

function insertReference(position, publicationId, citeText) {
	var publication = getPublicationFromCache(publicationId);
	if (publication == undefined)
		return;
	
	var row = position.appendTableRow();
	var cell = row.appendTableCell(citeText);
	cell.setWidth(30);
	cell.setPaddingBottom(0);
	cell.setPaddingTop(0);

	// create a bookmark for this reference
	var doc = DocumentApp.getActiveDocument();
	var pos = doc.newPosition(cell, 0);
	var bookmark = doc.addBookmark(pos);
	bookmarkReferences[citeText] = bookmark.getId();

	var cell2 = row.appendTableCell("");
	cell2.setPaddingBottom(0);
	cell2.setPaddingTop(0);

	position = cell2.editAsText();

	var abbreviate = getAbbreviateFirstName() == 'true';

	// Python Script https://bitbucket.org/bibsonomy/bibsonomy-python/overview
	// Copyright 2014 Robert Jäschke
	insertBetter(position, publication["title"] + '\r', true, false);
	if (abbreviate == false && publication["author"] != undefined) {
		insertBetter(position, publication["author"], false, true);
	} else if (abbreviate == true && publication["authorShort"] != undefined) {
		insertBetter(position, publication["authorShort"], false, true);
	} else if (publication["editor"] != undefined) {
		insertBetter(position, publication["editor"], false, true);
	}

	// Hyperlinks are not included
	if (publication["entrytype"] == "article") {
		if (publication["journal"] != undefined) {
			insertBetter(position, " " + publication["journal"], false, false);
		}
		if (publication["volume"] != undefined) {
			insertBetter(position, " " + publication["volume"] + " ", false,
					false);
		}
		if (publication["number"] != undefined) {
			insertBetter(position, " (" + publication["number"] + ") ", false,
					false);
		}
		if (publication["pages"] != undefined) {
			insertBetter(position, " " + publication["pages"] + " ", false,
					false);
		}
	} else if (publication["entrytype"] == "inproceedings"
			|| publication["entry_type"] == "incollection"
			|| publication["entry_type"] == "inbook") {
		if (publication["booktitle"] != undefined) {
			insertBetter(position, ", " + publication["booktitle"], false,
					false);
			if (publication["pages"] != undefined) {
				insertBetter(position, ", " + publication["pages"], false,
						false);
			}
		}
		if (publication["publisher"] != undefined) {
			if (publication["booktitle"] != undefined) {
				insertBetter(position, ", ", false, false);
			}
			insertBetter(position, publication["publisher"], false, false);
			if (publication["address"] != undefined) {
				insertBetter(position, ", " + publication["address"], false,
						false);
			}
		} else if (publication["organization"] != undefined) {
			if (publication["booktitle"] != undefined) {
				insertBetter(position, ", ", false, false);
			}
			insertBetter(position, publication["organization"], false, false);
			if (publication["address"] != undefined) {
				insertBetter(position, ", " + publication["address"], false,
						false);
			}
		}
	} else if (publication["entrytype"] == "book"
			|| publication["entry_type"] == "booklet"
			|| publication["entry_type"] == "proceedings") {
		if (publication["publisher"] != undefined) {
			insertBetter(position, publication["publisher"], false, false);
		}
	} else if (publication["entrytype"] == "mastersthesis"
			|| publication["entry_type"] == "phdthesis") {
		if (publication["school"] != undefined) {
			insertBetter(position, publication["school"], false, false);
			if (publication["address"] != undefined) {
				insertBetter(position, ", " + publication["address"], false,
						false);
			}
		}
	} else if (publication["entrytype"] == "techreport") {
		if (publication["type"] != undefined) {
			insertBetter(position, publication["type"], false, false);
		} else {
			insertBetter(position, "Technical Report", false, false);
		}
		if (publication["number"] != undefined) {
			insertBetter(position, " " + publication["number"], false, false);
		}
		if (publication["institution"] != undefined) {
			insertBetter(position, ", " + publication["institution"], false,
					false);
		}
	} else if (publication["entrytype"] == "manual") {
		if (publication["organization"] != undefined) {
			insertBetter(position, publication["organization"], false, false);
		}
		if (publication["address"] != undefined) {
			if (publication["organization"] != undefined) {
				insertBetter(position, ", ", false, false);
			}
			insertBetter(position, publication["address"], false, false);
		}
	}
	insertBetter(position, " (" + publication["year"] + ") ", false, false);

	if (publication["abstract"] != undefined) {
		insertBetter(position, "  <div class='abstract'>"
				+ post.resource.abstract + "</div>", false, false);
	}

}

/**
 * Inserts the citation at the current cursor location in boldface.
 */
function insertAtCursor(publication) {
	var locked = getLockStatus();
	if (locked == 'true') {
		DocumentApp
				.getUi()
				.alert(
						"Another user is currently working on the citations, please wait a few seconds and try again.");
		return false;
	} else {

		Locking.prototype.lock();

		var cursor = DocumentApp.getActiveDocument().getCursor();
		if (cursor) {
			// Attempt to insert text at the cursor position. If insertion
			// returns null,
			// then the cursor's containing element doesn't allow text
			// insertions.
			cursor.insertText(" ");
			var id = encodeDocumentId(publication);
			var element = cursor.insertText(getCiteNumber(id));

			if (element) {
				var rangeBuilder = DocumentApp.getActiveDocument().newRange();
				rangeBuilder.addElement(element, 0,
						element.getText().length - 1);
				// Maximal noch 200 Zeichen. Insgesamt 248 Zeichen
				DocumentApp.getActiveDocument().addNamedRange(id, rangeBuilder.build());

				addPublicationToCache(publication);
			} else {
				DocumentApp.getUi().alert(
						'Cannot insert text at this cursor location.');
			}
		} else {
			DocumentApp.getUi().alert('Cannot find a cursor in the document.');
		}

		Locking.prototype.unlock();
		return true;
	}
}

function getBibliographyTableElement() {
	var citationSource = DocumentApp.getActiveDocument().getNamedRanges(
			'BibSonomySourceCitation')
	var citationNamedRange;
	bookmarkReferences = {};
	if (citationSource.length == 0) {
		var body = DocumentApp.getActiveDocument().getBody();

		// Build a table from the array.
		body.appendParagraph("Quellenverzeichnis")
		var citationTable = body.appendTable();
		citationTable.setBorderWidth(0);

		var rangeBuilder = DocumentApp.getActiveDocument().newRange();
		rangeBuilder.addElement(citationTable);
		citationNamedRange = DocumentApp.getActiveDocument().addNamedRange(
				'BibSonomySourceCitation', rangeBuilder.build());
	} else {
		citationNamedRange = citationSource[0];
		if (citationNamedRange.getRange().getRangeElements().length == 0) {
			citationNamedRange.remove();
			return createReferences();
		}
	}

	return citationNamedRange.getRange().getRangeElements()[0].getElement()
			.asTable();
}

