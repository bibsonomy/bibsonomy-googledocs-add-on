/* 
 * To include this script into the GoogleDocs script editor you have to
 * add the first line to the top and the second line to the end of this file:
 * 
 * 1. <script>
 * 2. </script>
 */

/**
 * This function returns the documents in this node.
 * 
 * @returns The documents in this node.
 */
TreeIndexNode.prototype.getDocuments = function() {
	var documents = this.documents;
	if (this.root.searchForPartialWords == false) {
		return documents;
	} else {
		return documents.concat(this.childDocuments);
	}
};

/**
 * This function adds the document to this nodes document list. Also this adds
 * the document to the roots document list.
 * 
 * @param {String}
 *            interhash - The interhash to add to this node.
 * @returns {Boolean} True if the document interhash was added, false if it was
 *          already in the list.
 */
TreeIndexNode.prototype.addDocument = function(interhash) {
	if (this.documents.indexOf(interhash) == -1) {
		this.documents.push(interhash);
		this.root.addDocument(interhash);
		return true;
	}
	return false;
};

/**
 * This function adds the document to the nodes child documents list.
 * 
 * @returns {Boolean} True if the document interhash was added, false if it was
 *          already in the list.
 */
TreeIndexNode.prototype.addChildDocument = function(interhash) {
	if (this.childDocuments.indexOf(interhash) == -1) {
		this.childDocuments.push(interhash);
		return true;
	}
	return false;
};

/**
 * Constructor for TreeIndexNode.
 * 
 * @returns {TreeIndexNode} The new TreeIndexNode object.
 */
function TreeIndexNode(root) {
	this.root = root;
	this.documents = [];
	this.childDocuments = [];
}