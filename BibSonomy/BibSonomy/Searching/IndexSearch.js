/* 
 * To include this script into the GoogleDocs script editor you have to
 * add the first line to the top and the second line to the end of this file:
 * 
 * 1. <script>
 * 2. </script>
 */

/**
 * Shows only the entries matching the given search term. Removes non-word
 * characters and transforms the string into lower case.
 * 
 * @param {TreeIndex}
 *            index - The index to search in.
 * @param {String}
 *            searchTerms - String of search terms.
 * @returns {Array} Array of matching documents.
 */
IndexSearch.prototype.search = function(index, searchTerms) {
	searchTerms = searchTerms.toLowerCase();
	var termList = searchTerms.match(/\w+/g); // remove all non-word
	// characters
	if (termList == null) {
		return index.getAllDocuments();
	} else {
		return index.retrieveMatchingDocuments(termList);
		showMatchingDocuments(matchingDocuments);
	}
};

/**
 * Constructor for IndexSearch.
 * 
 * @returns {IndexSearch} The new IndexSearch object.
 */
function IndexSearch() {
};