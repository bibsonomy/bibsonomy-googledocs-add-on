/**
 * Shows only the entries matching the given search term.
 */
DelayedSearch.prototype.search = function(index, searchTerms) {
	var newTimestamp = new Date().getTime();
	if (newTimestamp - this.timestamp < this.searchDelay)
		return undefined;
	var searchTerm = $('#search').val(); // get search term
	searchTerm = searchTerm.toLowerCase();
	var termList = searchTerm.match(/\w+/g); // remove all non-word
												// characters
	if (termList == null) { // if there is no search term then show all
							// documents
		return index.getAllDocuments();
	} else { // else send search request
		return index
				.retrieveMatchingDocuments(termList);
		showMatchingDocuments(matchingDocuments);
	}
};

function delayedSearch() {
	this.timestamp = new Date().getTime();
	window.setTimeout(this.search, this.searchDelay);
};

/**
 * Constructor for DelayedSearch.
 * @returns {DelayedSearch}
 */
function DelayedSearch() {
	this.searchDelay = 200;
	this.timestamp = 0;
};