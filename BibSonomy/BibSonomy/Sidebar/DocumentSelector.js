DocumentSelector.prototype.selectDocuments = function(documents) {
	for ( var document in documents) {
		this.selectDocument(document)
	}
}

DocumentSelector.prototype.deselectDocuments = function(documents) {
	for ( var document in documents) {
		this.deselectDocument(document)
	}
}

DocumentSelector.prototype.deselectAll = function() {
	this.selectedDocuments = [];
}

DocumentSelector.prototype.selectDocument = function(document) {
	this.selectedDocuments.push(document);
}

DocumentSelector.prototype.deselectDocument = function(document) {
	this.selectedDocuments.pop(document);
}

DocumentSelector.prototype.getSelectedDocuments = function() {
	return this.selectedDocuments;
}

DocumentSelector.prototype.getSelectedDocumentsUIComponents = function() {
	var components = [];
	for ( var document in this.selectedDocuments) {
		components.push(this.data[document].separator);
		components.push(this.data[document].text);
	}
	return components;
}

DocumentSelector.prototype = new InterhashToUIMap();
DocumentSelector.prototype.constructor = DocumentSelector;

/**
 * Constructor for DocumentSelector.
 * 
 * @returns {DocumentSelector} The new DocumentSelector object.
 */
function DocumentSelector() {
	InterhashToUIMap.call(this);
	this.selectedDocuments = [];
}