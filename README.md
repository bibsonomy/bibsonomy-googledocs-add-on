# BibSonomy GoogleDocs Add-on

The BibSonomy GoogleDocs add-on helps inserting citations into a document in a convenient way. 
The add-on uses the data available on BibSonomy to access your bookmarks. It also features 
searching with automatic completion to make it easier to find the required citation source.

More information can be found in [the Wiki](/fwhkoenig/bibsonomy-googledocs-add-on/wiki/Home).